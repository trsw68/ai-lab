import processing.core.PGraphics;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class Info implements Entity {

    private List<InfoDrawing> drawings;

    private int drawingPosition;

    private Info() {
        this.drawings = new ArrayList<>();
    }

    private static Info instance = new Info();


    public static Info getInstance() {
        return Info.instance;
    }

    public boolean finishedDrawingContent() {
        return this.drawings.size() == this.drawingPosition;
    }

    private void sleep() {
        if (this.drawingPosition > 0) {
            try {
                TimeUnit.MILLISECONDS.sleep(this.drawings.get(this.drawingPosition - 1).sleepTime);
            } catch (InterruptedException ignored) {}
        }
    }

    public void reset() {
        this.sleep();
        this.drawings.clear();
        this.drawingPosition = 0;
    }

    public void addDrawing(InfoDrawing visualization) {
        this.drawings.add(visualization);
    }

    @Override
    public void draw(PGraphics layer) {
        // sleep for the defined amount of time after the last drawing
        this.sleep();

        IntStream.rangeClosed(0, this.drawingPosition).forEach(drawing -> {
            InfoDrawing current = this.drawings.get(drawing);

            if (!current.isAnimation() || drawing == this.drawingPosition) {
                current.getBiConsumer().accept(layer, current);
            } else {
                current.getFinishedAnimation().accept(layer, current);
            }
        });

        this.drawingPosition++;
    }
}

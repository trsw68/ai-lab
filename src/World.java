import processing.core.PGraphics;
import processing.core.PVector;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;


/**
 * The world which contains the maze, a treasure and a generation of seekers.
 */
public class World implements Entity {

    /**
     * The maze of this world.
     */
    private final Maze maze;


    /**
     * The list of all currently existing Generations
     */
    private List<Generation> generations = new ArrayList<>();


    /**
     * Defines, whether the previous generation should be drawn.
     */
    private boolean showPreviousGeneration;


    /**
     * Contains the current active generation.
     */
    private Generation currentGeneration;


    /**
     * The visualized bar size of the best generation.
     */
    private float highestBar;


    private List<Color> barColors = new ArrayList<>();
    private List<Generation> skillBarHistory = new ArrayList<>();
    private boolean updateSkillHistoryView;
    private int noImprovements = -2;


    /**
     * The default world constructor.
     */
    public World() {
        this.maze = Config.MAZE;

        this.createNewGeneration();
    }


    /**
     * Creates a new seeker generation on call.
     */
    public void createNewGeneration() {
        this.updateSkillHistoryView = true;
        this.noImprovements++;
        this.currentGeneration = (this.currentGeneration == null)
            ? new Generation(this.maze.getSeekersStartPoint())
            : this.currentGeneration.createNextGeneration(this.maze)
        ;
        this.generations.add(this.currentGeneration);
        // free up memory - track only 50 past generations
        if (this.generations.size() > 50) {
            this.generations.remove(0);
            for (Seeker seeker : this.generations.get(0).getSeekers()) {
                seeker.unbindAncestor();
            }
        }
    }


    /**
     * Returns the current active generation.
     */
    public Generation currentGeneration() {
        return this.currentGeneration;
    }


    public void showPreviousGeneration(boolean showPreviousGeneration) {
        this.showPreviousGeneration = showPreviousGeneration;
    }


    /**
     * Returns the width of the world based on its maze.
     */
    public int width() {
        return this.maze.width() * 2;
    }


    /**
     * Returns the height of the maze plus space for world information.
     */
    public int height() {
        return this.maze.height();
    }


    /**
     * Update world entities on call.
     */
    public void update() {
        boolean generationDead = true;
        PVector nextStep;

        // update all seeker movements of the current generation
        for (Seeker seeker: this.currentGeneration.getSeekers()) {

            if (seeker.isAlive()) { // only alive seekers will be further checked
                nextStep = seeker.getNextStep();

                // kill seeker if treasure found
                if (this.maze.getTileGrade(nextStep) == 0) {
                    seeker.update();
                    seeker.kill();
                } else {
                    generationDead = false;
                    // verify if next step would hit a maze wall
                    if (!this.maze.collides(nextStep)) {
                        seeker.update();
                    } else {
                        seeker.notifyWallHit();
                    }
                }
            }
        }

        if (generationDead) {
            this.currentGeneration.kill();
        }
    }


    /**
     * Returns the information y offset.
     */
    public int getInformationYOffset() {
        return 30;
    }


    //====================================================================================================
    //-------------------- Additional optional visualization methods below            --------------------
    //====================================================================================================

    /**
     * Returns the skill bar height of the given generation.
     */
    private float calculateBarHeight(Generation generation) {
        return (generation.getCumulativeSkill() / (Config.SEEKER_COUNT * 1.0f));
    }


    private void drawSeparators(PGraphics layer) {
        layer.noStroke();
        layer.fill(100);

        layer.rect(this.maze.width(), 0, 3, this.height());
        layer.rect((this.maze.width() + 440), 420, 3, this.maze.height());
        layer.rect(this.maze.width(), 420, this.maze.width(), 3);

        layer.stroke = true;
    }


    private void updateSkillHistoryView() {
        assert this.updateSkillHistoryView && this.generations.size() > 1;

        Generation current = this.generations.get(this.generations.size() - 2);
        if (this.generations.size() == 2) {
            // draw a black bar for the very first generation.
            this.barColors.add(new Color(0));
        } else {

            Generation old = this.generations.get(this.generations.size() - 3);
            float barHeight = this.calculateBarHeight(current);
            float oldBarHeight = this.calculateBarHeight(old);

            if (barHeight > this.highestBar) {
                // draw a green bar if the generation is the best in comparision to all previous generations.
                this.highestBar = barHeight;
                this.barColors.add(new Color(0, 255, 0));
            } else if (oldBarHeight > barHeight) {
                // draw a red bar if the generation is worse than the previous generation.
                this.barColors.add(new Color(255, 0, 0));
            } else if (oldBarHeight < barHeight) {
                // draw a yellow bar if the generation is better than the previous generation,
                // but worse than the best of all previous generations.
                this.barColors.add(new Color(255, 255, 0));
            } else {
                // draw a blue bar if the generation is neither better nor worse than the previous generation.
                this.barColors.add(new Color(0, 0, 255));
            }
        }

        this.skillBarHistory.add(current);

        if (this.barColors.size() > 10 || this.skillBarHistory.size() > 10) {
            assert this.barColors.size() == this.skillBarHistory.size();

            this.barColors.remove(0);
            this.skillBarHistory.remove(0);
        }

        this.updateSkillHistoryView = false;
    }


    /**
     * Draws the skill bar visualization.
     */
    private void drawSkillBars(PGraphics layer) {
        // show latest generation skills
        int defaultXOffset = (this.maze.width() + 40);
        int defaultYOffset = (this.maze.height() - 70);

        layer.text("Cumulative generation skill history:", defaultXOffset, 450);

        if (this.updateSkillHistoryView && this.generations.size() > 1) {
            this.updateSkillHistoryView();
        }

        float scale;
        float maxVisibleBarHeight = 0.0f;

        for (Generation generation : this.skillBarHistory) {
            float barHeight = this.calculateBarHeight(generation);
            if (barHeight > maxVisibleBarHeight) {
                maxVisibleBarHeight = barHeight;
            }
        }

        scale = maxVisibleBarHeight / (this.maze.height() - 540);

        // draw generation numbers + skill bars
        for (int i = (this.skillBarHistory.size() - 1), offset = 0; i >= 0; i--, offset++) {
            Generation generation = this.skillBarHistory.get(i);
            Color c = this.barColors.get(i);
            float barHeight = this.calculateBarHeight(generation);

            layer.fill(0);
            layer.text(generation.getNo(), defaultXOffset + (40 * offset), this.maze.height() - 40);

            layer.fill(c.getRed(), c.getGreen(), c.getBlue());
            layer.rect(defaultXOffset + 40 * offset, defaultYOffset - (barHeight / scale), 10, (barHeight / scale));
        }

          // Optional feature: scale representation

//        String scaleRepresentation = (scale == 0) ? "-" : "x" + (((1 / scale) + "    ").substring(0, 4));
//
//        layer.fill(0);
//        layer.text(("scale: " + scaleRepresentation), defaultXOffset, this.maze.height() - 10);
    }


    /**
     * Draws the best seeker's skill progress visualization.
     */
    private void drawBestSeekerProgress(PGraphics layer) {
        int xDefault = (this.maze.width() + 470);

        layer.fill(0);
        layer.text("Best seeker's skill: ", xDefault, 450);

        layer.text("current  |  (previous)", xDefault, 500);

        if (this.generations.size() > 1) {
            Generation current = this.generations.get(this.generations.size() - 1);
            int currentBest = current.getSeekers()[0].getAncestor().getSkill();

            if (this.generations.size() > 2) {
                Generation previous = this.generations.get(this.generations.size() - 2);
                int previousBest = previous.getSeekers()[0].getAncestor().getSkill();

                layer.stroke = false;

                if (currentBest > previousBest) {
                    layer.fill(0, 255, 0);
                    layer.triangle((xDefault - 10), 540, (xDefault - 15), 550, (xDefault - 5), 550);
                    this.noImprovements = 0;
                } else {
                    layer.fill(255, 165, 0);
                    layer.circle((xDefault - 10), 545, 10);
                }

                layer.stroke = true;

                layer.text(currentBest, xDefault, 550);

                layer.fill(0);
                layer.text(("(" + previousBest + ")"), (xDefault + 95), 550);

                // stagnation counter
                if (this.noImprovements == 0) {
                    layer.fill(0, 255, 0);
                    layer.text("No stagnation.", xDefault, 580);
                } else {
                    if (this.noImprovements < 20) {
                        layer.fill(255, 165, 0);
                    } else {
                        layer.fill(255, 0, 0);
                    }

                    layer.text(("Stagnation for " + this.noImprovements), xDefault, 580);
                    layer.text(((this.noImprovements == 1) ? "generation." : "generations."), xDefault, 610);
                }
            } else {
                layer.text(currentBest, xDefault, 550);
            }
        }
        layer.fill(0);
    }

    /**
     * Draws the treasure found message / visualization.
     */
    private void drawTreasureMessage(PGraphics layer) {
        layer.text("Target found:", (this.maze.width() + 470), 650);

        if (this.generations.size() > 1 && this.generations.get(this.generations.size() - 2).treasureFound()) {
            layer.fill(0, 255, 0);
        } else {
            layer.fill(255, 0, 0);
        }

        layer.stroke = false;

        layer.circle((this.maze.width() + 620), 642, 15);

        layer.stroke = true;
        layer.fill(0);
    }


    //====================================================================================================
    //-------------------- Additional optional visualization methods above            --------------------
    //====================================================================================================

    @Override
    public void draw(PGraphics layer) {
        Generation drawnGeneration = this.generations.get(this.generations.size() - (this.showPreviousGeneration
                ? 2
                : 1
        ));

        // draw separators
        this.drawSeparators(layer);

        // draw maze background
        layer.fill(198, 225, 164);
        layer.rect(0, 0, this.maze.width(), this.height());

        // draw generation message / number
        layer.fill(0);
        layer.textSize(20);
        layer.text(drawnGeneration.toString(), this.maze.width() + 20, this.getInformationYOffset());

        // draw the maze
        this.maze.draw(layer);

        // render all seekers
        for (Seeker seeker: drawnGeneration.getSeekers()) {
            seeker.draw(layer);
        }

        // draw the cumulative generation skill history bar diagram
        this.drawSkillBars(layer);

        // draw the best seeker difference
        this.drawBestSeekerProgress(layer);

        // draw the treasure found message
        this.drawTreasureMessage(layer);
    }
}

import processing.core.PGraphics;


/**
 * A simple entity.
 */
public interface Entity {

    /**
     * Draws the entity on call.
     */
    void draw(PGraphics layer);
}

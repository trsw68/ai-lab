import processing.core.PGraphics;
import processing.core.PVector;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;


/**
 * A Maze where seekers, treasures and walls are placed in.
 */
public class Maze implements Entity {

    /**
     * Available mazes:
     */
    public static final Maze SINGLE_PATH          = Maze.createMazeFromFile("singlePath");
    public static final Maze MULTI_PATH           = Maze.createMazeFromFile("multiPath");
    public static final Maze ANGULAR              = Maze.createMazeFromFile("angular");
    public static final Maze OPEN_SPACE           = Maze.createMazeFromFile("openSpace");
    public static final Maze OPEN_SPACE_WITH_WALL = Maze.createMazeFromFile("openSpaceWithWall");


    /**
     * The tiles of the mazes.
     */
    private MazeTile[][] tiles;


    /**
     * The seekers start point coordinate.
     */
    private PVector seekersStartPoint;


    /**
     * The treasure position vector.
     */
    private PVector treasurePosition;


    /**
     * The grades of the tiles which contains the distance between the added tile and the treasure of the tile.
     */
    private Map<String, Integer> tileGrades = new HashMap<>();


    /**
     * The default constructor with height and width information.
     */
    private Maze(MazeTile[][] tiles, PVector seekersStartPoint, PVector treasurePosition) {
        assert seekersStartPoint != null && treasurePosition != null;

        this.tiles = tiles;
        this.seekersStartPoint = seekersStartPoint;
        this.treasurePosition = treasurePosition;

        this.createTileGrades(treasurePosition);
    }


    /**
     * Creates the grades of tiles relative to the treasure position.
     */
    private void createTileGrades(PVector treasurePosition) {
        this.assignGrade(0, treasurePosition);
    }


    /**
     * Assigns a grade to the tile coordinate and recursively add higher grades to neighbor tiles.
     */
    private void assignGrade(int grade, PVector tileCoordinate) {
        int x = (int) tileCoordinate.x;
        int y = (int) tileCoordinate.y;

        // only valid coordinates are allowed
        if (
            x >= 0 && y >= 0 &&
            y < this.tiles.length &&
            x < this.tiles[0].length &&
            this.tiles[y][x].getTileType() != MazeTileType.WALL
        ) {
            String identifier = (x + "|" + y);

            // do not update lower existing grades
            if (this.tileGrades.containsKey(identifier) && this.tileGrades.get(identifier) <= grade) {
                return;
            }

            // try to apply grade to all 4 directions
            this.tileGrades.put(identifier, grade);

            grade += 1;

            this.assignGrade(grade, PVector.add(tileCoordinate, new PVector(0, -1))); // top
            this.assignGrade(grade, PVector.add(tileCoordinate, new PVector(1, 0))); // right
            this.assignGrade(grade, PVector.add(tileCoordinate, new PVector(0, 1))); // bottom
            this.assignGrade(grade, PVector.add(tileCoordinate, new PVector(-1, 0))); // left
        }
    }


    /**
     * Returns the start point of the seekers in the maze.
     */
    public PVector getSeekersStartPoint() {
        return seekersStartPoint.copy();
    }


    /**
     * Returns the width of the maze.
     */
    public int width() {
        return this.tiles[0].length * Config.MAZE_TILE_DIMENSION;
    }


    /**
     * Returns the height of the maze.
     */
    public int height() {
        return this.tiles.length * Config.MAZE_TILE_DIMENSION;
    }


    /**
     * Returns TRUE if the provided coordinate collides with an element of the maze, FALSE otherwise.
     */
    public boolean collides(PVector coordinate) {
        int absY = (int) (coordinate.y / Config.MAZE_TILE_DIMENSION);

        // verify that coordinate is in y axis of maze
        if (absY >= 0 && absY < this.tiles.length) {
            int absX = (int) (coordinate.x / Config.MAZE_TILE_DIMENSION);

            // verify that coordinate is in x axis of maze
            if (absX >= 0 && absX < this.tiles[absY].length) {
                return (this.tiles[absY][absX].hasCollision());
            }
        }

        return true;
    }


    /**
     * Returns the tile grade of the provided coordinate. If no coordinate is
     */
    public int getTileGrade(PVector coordinate) {
        return this.tileGrades.getOrDefault(
            ((int) coordinate.x / Config.MAZE_TILE_DIMENSION) + "|" + ((int) coordinate.y / Config.MAZE_TILE_DIMENSION),
            -1
        );
    }


    /**
     * Creates and returns a maze object based on the maze definition in target file.
     */
    private static Maze createMazeFromFile(String fileName) {
        Path mazePath = Path.of("src/mazes/" + fileName + ".maze");
        boolean treasureSet = false;
        Character[][] mazeAsChars;
        MazeTile[][] tiles;
        PVector seekersStartPoint = null;
        PVector treausePosition = null;

        // Create Character[][] representation of file
        try {
            mazeAsChars = Files.lines(mazePath)
                .map(s -> s.chars()
                    .mapToObj(i -> (char) i)
                    .toArray(Character[]::new)
                )
                .toArray(Character[][]::new)
            ;
        } catch (Exception e) {
            throw new IllegalArgumentException(e.toString());
        }

        // create tile array from maze height and width
        tiles = new MazeTile[mazeAsChars.length][mazeAsChars[0].length];

        // create tile rows
        for (int y = 0; y < tiles.length; y++) {
            // and columns
            for (int x = 0; x < tiles[y].length; x++) {
                MazeTileType type = null;

                switch (mazeAsChars[y][x]) {
                    case '#':
                        type = MazeTileType.WALL;
                        break;

                    case 'S':
                        // only one start point is allowed
                        if (seekersStartPoint != null) {
                            throw new IllegalStateException("Attempt to set seekerStartPoint multiple times");
                        }

                        seekersStartPoint = new PVector(x * Config.MAZE_TILE_DIMENSION, y * Config.MAZE_TILE_DIMENSION);
                        type = MazeTileType.SEEKERS_START_POINT;
                        break;

                    case '.':
                        type = MazeTileType.PATH;
                        break;

                    case 'T':
                        // only one treasure is allowed in the maze
                        if (treasureSet) {
                            throw new IllegalStateException("Attempt to set treasure multiple times");
                        }

                        treausePosition = new PVector(x, y);
                        type = MazeTileType.TREASURE;
                        treasureSet = true;
                        break;
                }

                // invalid maze tiles are not allowed
                if (type == null) {
                    throw new IllegalStateException("MazeTileType not found for char " + mazeAsChars[y][x]);
                }

                tiles[y][x] = new MazeTile(type, x * Config.MAZE_TILE_DIMENSION, y * Config.MAZE_TILE_DIMENSION);
            }
        }

        // a treasure and seekers start point must always be available
        if (!treasureSet || seekersStartPoint == null) {
            throw new IllegalStateException("Treasure or seekerStartPoint is not set. Please adapt maze definition.");
        }

        return new Maze(tiles, seekersStartPoint, treausePosition);
    }


    @Override
    public void draw(PGraphics layer) {
        // draw maze rows
        for (MazeTile[] tiles: this.tiles) {
            // and columns
            for (MazeTile tile: tiles) {
                tile.draw(layer);
            }
        }
    }
}

import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PVector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


/**
 * A seeker who does everything to find the treasure.
 */
public class Seeker implements Entity {

    /**
     * A simple static randomization object.
     */
    private static final Random random = new Random();


    /**
     * The image of the seeker.
     *
     * By default its a bee, but some bees are special.
     */
    private PImage image;


    /**
     * The ancestor of the seeker.
     */
    private Seeker ancestor;


    /**
     * The starting position of the seeker.
     */
    private final PVector startingPosition;


    /**
     * The current position of the seeker.
     */
    private PVector position;


    /**
     * The current velocity of the seeker.
     */
    private PVector velocity = new PVector(0, 0);


    /**
     * A list of movement intentions of the seeker.
     */
    private PVector[] movementIntentions;


    /**
     * Contains the movement intention index of the seeker on first wall hit.
     */
    private List<Integer> wallHitPositions = new ArrayList<>();


    /**
     * The total move count of the seeker.
     */
    private int movesDone;


    /**
     * TRUE if the seeker is flagged as dead like after all move intentions are processed.
     */
    private boolean dead;


    /**
     * The assigned skill of the seeker.
     */
    private int skill;


    /**
     * TRUE, if the seeker is the best one; FALSE otherwise.
     */
    public boolean bestSeeker;


    /**
     * A seeker constructor with its starting position and generated movement intentions.
     */
    public Seeker(PVector startingPosition, PVector[] movementIntentions) {
        this(startingPosition, movementIntentions, Config.SEEKER_DEFAULT_SKILL);
    }


    public Seeker(PVector startingPosition, PVector[] movementIntentions, Seeker ancestor) {
        this(startingPosition, movementIntentions, Config.SEEKER_DEFAULT_SKILL, ancestor);
    }


    /**
     * A seeker constructor with its starting position, its movement intention and its skill.
     */
    public Seeker(PVector startingPosition, PVector[] movementIntentions, int skill) {
        this(startingPosition, movementIntentions, skill, null);
    }


    public Seeker(PVector startingPosition, PVector[] movementIntentions, int skill, Seeker ancestor) {
        this.ancestor = ancestor;
        this.startingPosition = startingPosition;
        this.position = startingPosition.copy();
        this.movementIntentions = movementIntentions;
        this.skill = skill;
        this.image = Images.getInstance().getImage("bee");
    }


    /**
     * Generates and returns the desired new position of the seeker.
     */
    public PVector getNextStep() {
        // store velocity for applying it on update() method if next step is a valid one
        this.velocity.add(this.movementIntentions[this.movesDone++]);
        this.velocity.limit(Config.SEEKER_MAX_VELOCITY);

        // flag seeker as dead
        if (this.movesDone == this.movementIntentions.length) {
            this.kill();
        }

        return this.position.copy().add(this.velocity);
    }


    /**
     * Notifies the seeker about a wall hit, so he can react to it.
     */
    public void notifyWallHit() {
        this.wallHitPositions.add(this.movesDone - 1);
    }


    /**
     * Returns the wall hit positions.
     */
    public List<Integer> getWallHitPositions() {
        return List.copyOf(this.wallHitPositions);
    }


    /**
     * Updates the position of the seeker with the current assigned velocity.
     */
    public void update() {
        this.position.add(this.velocity);
    }


    /**
     * Returns the current position of the seeker.
     */
    public PVector getPosition() {
        return this.position.copy();
    }


    /**
     * Returns the starting position of the seeker.
     */
    public PVector getStartingPosition() {
        return this.startingPosition.copy();
    }


    /**
     * Returns the skill of the seeker.
     */
    public int getSkill() {
        assert this.skill > 0;
        return this.skill;
    }


    /**
     * Sets the skill of the seeker.
     */
    public void setSkill(int value) {
        this.skill = value;
    }


    /**
     * Returns the number of moves executed by the seeker until e.g. treasure was found or generation ended.
     */
    public int getMoveCount() {
        return this.movesDone;
    }


    /**
     * Returns the ancestor move count.
     */
    public int getAncestorMoveCount() {
        return this.ancestor.getMoveCount();
    }


    public Seeker getAncestor() {
        return this.ancestor;
    }


    /**
     * Unbinds the ancestor from the seeker to free up memory.
     */
    public void unbindAncestor() {
        this.ancestor = null;
    }


    /**
     * Returns TRUE if the seeker is not flagged as dead, FALSE otherwise.
     */
    public boolean isAlive() {
        return !this.dead;
    }


    /**
     * Kills the seeker.
     */
    public void kill() {
        assert isAlive();
        this.dead = true;
    }


    /**
     * Generates and returns a clone of the seeker.
     */
    public Seeker generateClone() {
        return new Seeker(
            this.getStartingPosition(),
            Arrays.stream(this.movementIntentions)
                .map(PVector::copy)
                .toArray(PVector[]::new),
            this.skill,
            this
        );
    }


    /**
     * Creates and returns a random move intention vector.
     */
    public static PVector createRandomMoveIntent() {
        return new PVector(
                // creates a value between the max positive and max negative move intention
                (random.nextFloat() * Config.SEEKER_MAX_MOVE_INTENTION * 2f) - Config.SEEKER_MAX_MOVE_INTENTION,
                (random.nextFloat() * Config.SEEKER_MAX_MOVE_INTENTION * 2f) - Config.SEEKER_MAX_MOVE_INTENTION
        );
    }


    /**
     * Returns the seekers movement intentions.
     */
    public PVector[] getMovementIntentions() {
        return this.movementIntentions;
    }


    /**
     * Allows to update the movement intentions of the seeker.
     */
    public void setMovementIntentions(PVector[] intentions) {
        this.movementIntentions = intentions;
    }


    @Override
    public void draw(PGraphics layer) {
        if (this.bestSeeker) {
            layer.fill(255, 0, 0);
            layer.circle(this.position.x - 2, this.position.y - 4, 12);
            layer.fill(0);
        }

        layer.image(this.image, this.position.x - 5, this.position.y - 5);
    }


    @Override
    public String toString() {
        return "Seeker (" + this.getSkill() + ")";
    }
}

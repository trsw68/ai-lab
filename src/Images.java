import processing.core.PImage;

import java.util.HashMap;
import java.util.Map;


/**
 * Provides image for rendering.
 */
public class Images {

    /**
     * The images instance.
     */
    private static Images instance = new Images();


    /**
     * Contains all assigned images
     */
    private Map<String, PImage> images = new HashMap<>();


    /**
     * Returns the images instance.
     */
    public static Images getInstance() {
        return Images.instance;
    }


    /**
     * Adds an image.
     */
    public void addImage(PImage image, String name) {
        this.images.put(name, image);
    }


    /**
     * Returns the image.
     */
    public PImage getImage(String name) {
        return this.images.get(name);
    }
}

import processing.core.PGraphics;
import processing.core.PImage;


/**
 * A maze tile can be placed insides mazes and can be of different types like a wall, a way or even a treasure.
 */
public class MazeTile implements Entity {

    /**
     * The x position of the tile.
     */
    private final int x;


    /**
     * The y position of the tile.
     */
    private final int y;


    /**
     * The maze file type.
     */
    private final MazeTileType type;


    /**
     * The image of the maze tile.
     */
    private PImage image;


    /**
     * The tile image width;
     */
    private int imageWidth  = Config.MAZE_TILE_DIMENSION;


    /**
     * The tile image height;
     */
    private int imageHeight = Config.MAZE_TILE_DIMENSION;


    /**
     * The maze tile constructor with its type and x and y coordinate.
     */
    public MazeTile(MazeTileType type, int x, int y) {
        this.type = type;

        switch (type) {
            case TREASURE:
                this.image = Images.getInstance().getImage("treasure");
                this.imageWidth  = 20;
                this.imageHeight = 20;
                this.x = x - (Config.MAZE_TILE_DIMENSION / 2);
                this.y = y - (Config.MAZE_TILE_DIMENSION / 2);
                break;

            case WALL:
                this.image = Images.getInstance().getImage("tree" + (((int) (Math.random() * 3)) + 1));
            default:
                this.x = x;
                this.y = y;
        }
    }


    /**
     * Returns TRUE if the maze tile provides a collision for detection, FALSE otherwise.
     */
    public boolean hasCollision() {
        return this.type.equals(MazeTileType.WALL);
    }


    /**
     * Returns the tile type.
     */
    public MazeTileType getTileType() {
        return this.type;
    }


    @Override
    public void draw(PGraphics layer) {
        // render the tile image if defined
        if (this.image != null) {
            layer.image(this.image, this.x, this.y, this.imageWidth, this.imageHeight);
        }
    }
}

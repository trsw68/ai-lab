import processing.core.PGraphics;
import processing.core.PVector;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.LongUnaryOperator;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.IntStream;


/**
 * A generation of seekers.
 */
public class Generation {


    /**
     * The generation number;
     */
    private int no;


    /**
     * A list of seekers in this generations.
     */
    private Seeker[] seekers;


    /**
     * The maze where the generation of seekers will search in for the treasure.
     */
    private PVector startPosition;


    /**
     * The status of the generation.
     */
    private boolean isDead;


    /**
     * TRUE, if at least one of the generation's seekers has found the treasure.
     */
    private boolean treasureFound;


    /**
     * The cumulative skill of all seekers in the generation.
     */
    private long generationSkill;


    /**
     * A generation constructor without previous generation information.
     */
    public Generation(PVector startPosition) {
        this(1, startPosition);
    }


    /**
     * The generation constructor with a number and the start position of the seekers.
     */
    public Generation(int no, PVector startPosition) {
        this(
            no,
            startPosition,
            IntStream.range(0, Config.SEEKER_COUNT)
                .mapToObj(seekerNo -> new Seeker(
                    startPosition.copy(),
                    Generation.generateSeekerMoveIntentions()
                ))
                .toArray(Seeker[]::new)
        );
    }


    /**
     * A generation constructor
     */
    public Generation(int no, PVector startPosition, Seeker[] seekers) {
        this.no            = no;
        this.startPosition = startPosition.copy();
        this.seekers       = seekers;
    }


    /**
     * Returns the generation number.
     */
    public int getNo() {
        return this.no;
    }


    /**
     * Returns an array of seekers of the generation.
     */
    public Seeker[] getSeekers() {
        return this.seekers;
    }


    public long getCumulativeSkill() {
        return this.generationSkill;
    }


    public boolean treasureFound() {
        return this.treasureFound;
    }


    public boolean isDead() {
        return this.isDead;
    }


    public void kill() {
        this.isDead = true;
    }


    /**
     * Creates and returns a list of seeker accelerations with the provided move count.
     */
    public static PVector[] generateSeekerMoveIntentions() {
        PVector[] moves = new PVector[Config.SEEKER_MOVE_COUNT];

        // create moves
        for (int i = 0; i < moves.length; i++) {
            moves[i] = Seeker.createRandomMoveIntent();
        }

        return moves;
    }


    /**
     * Creates and returns the next generation of seekers relative to the current one.
     */
    public Generation createNextGeneration(Maze maze) {
        LongUnaryOperator seekerIndexGenerator  = skillValue -> (long) ((Math.random() * skillValue) + 1);
        UnaryOperator<Seeker> bestSeekerMutator = (seeker -> {
            Seeker clone                        = seeker.generateClone();
            PVector[] movements                 = clone.getMovementIntentions();
            boolean workOnAllMovements          = Math.random() < 0.2;
            List<PVector> movementsAsList       = new ArrayList<>(Arrays.asList(movements));
            int removedMoves                    = 0;

            // optimize wall hitting
            for (int wallHitPosition : clone.getWallHitPositions()) {
                int index = wallHitPosition - removedMoves;
                System.out.println("removed: " + index);
                movementsAsList.remove(index);
                removedMoves++;
            }

            Predicate<Integer> modificationCondition = (
                    i -> ((i < movementsAsList.size()) &&
                            (workOnAllMovements ||
                                    (i < (Config.SEEKER_MOVE_COUNT / 4)) ||
                                    (i >= (Config.SEEKER_MOVE_COUNT - (Config.SEEKER_MOVE_COUNT / 4)))
                            )
                    )
            );

            // Remove some (early + late) movements to improve
            for (int i = 0; modificationCondition.test(i); i++) {
                if (Math.random() < Config.DEFAULT_SEEKER_PERMUTATION_RATE) {
                    movementsAsList.remove(i);
                }
            }

            // fill up with new random moves
            while (movementsAsList.size() < Config.SEEKER_MOVE_COUNT) {
                movementsAsList.add(Seeker.createRandomMoveIntent());
            }

            movements = movementsAsList.toArray(PVector[]::new);

            clone.setMovementIntentions(movements);

            return clone;
        });
        UnaryOperator<Seeker> trailingSeekersMutator = (seeker -> {
            Seeker clone = seeker.generateClone();
            PVector[] movementIntentions  = clone.getMovementIntentions();

            // apply random movement mutations until treasure is found
            for (int i = 0; i < movementIntentions.length; i++) {
                if (Math.random() < Config.DEFAULT_SEEKER_PERMUTATION_RATE) {
                    movementIntentions[i] = Seeker.createRandomMoveIntent();
                }
            }

            return clone;
        });

        // calculate individual skills for each seeker in the generation
        this.calculateSkills(maze);

        // create the seekers of the next generation
        Seeker[] nextGenSeekers = this.createNextGenerationOfSeekers(
                seekerIndexGenerator,
                bestSeekerMutator,
                trailingSeekersMutator
        );

        return new Generation(this.getNo() + 1, this.startPosition.copy(), nextGenSeekers);
    }


    /**
     * Calculates the skill of each seeker of the maze and checks if a seeker has found the treasure.
     */
    private void calculateSkills(Maze maze) {
        final int DEFAULT_SKILL = (1000 * (maze.width() + maze.height()) / Config.MAZE_TILE_DIMENSION);
        int distance;
        int yOffset = 0;
        Info info = Info.getInstance();

        if (Config.SHOW_INFO) {
            info.addDrawing(new InfoDrawing(
                    15,
                    Config.INFO_HEADLINE_Y,
                    (canvas, infoDrawing) -> {
                        canvas.textSize(Config.INFO_DEFAULT_TEXT_SIZE);
                        canvas.text("Skills:", infoDrawing.x, infoDrawing.y);
                    }
            ));
        }

        // assigns skills to seekers
        for (Seeker seeker: this.getSeekers()) {
            distance = maze.getTileGrade(seeker.getPosition());

            // verify incorrect seeker distances
            if (distance < 0) {
                throw new IllegalStateException("Distance < 0");
            } else if (distance == 0) {
                this.treasureFound = true;
            }

            int skill = (int) (DEFAULT_SKILL * (Math.pow(Math.E, -(distance/15.0)))); // before: 45.0 / 10.0

            seeker.setSkill((distance != 0)
                ? skill
                : (((DEFAULT_SKILL * 100) / (seeker.getMoveCount() * 10)) + (2 * skill)) // treasure found bonus
            );


            // Visualization
            if (Config.SHOW_INFO) {

                if (yOffset <= 10) {
                    BiConsumer<PGraphics, InfoDrawing> skillDisplayBiConsumer = (canvas, infoDrawing) -> {
                        canvas.textSize(Config.INFO_DEFAULT_TEXT_SIZE);
                        canvas.text("(" + seeker.getSkill() + ")", infoDrawing.x, infoDrawing.y);
                    };

                    info.addDrawing(new InfoDrawing(
                            35,
                            yOffset * Config.VISUALIZATION_OFFSET_FACTOR,
                            skillDisplayBiConsumer
                    ));

                } else if (yOffset == 11) {
                    BiConsumer<PGraphics, InfoDrawing> threeDotsBelow = (canvas, infoDrawing) -> {
                        canvas.fill(0);
                        canvas.circle(infoDrawing.value(0), infoDrawing.y, Config.INFO_DEFAULT_SIZE);
                        canvas.circle(infoDrawing.value(1), infoDrawing.y, Config.INFO_DEFAULT_SIZE);
                        canvas.circle(infoDrawing.value(2), infoDrawing.y, Config.INFO_DEFAULT_SIZE);
                    };

                    info.addDrawing(new InfoDrawing(
                            -1,
                            yOffset * Config.VISUALIZATION_OFFSET_FACTOR,
                            threeDotsBelow,
                            40, 50, 60
                    ));
                }

                yOffset++;
            }
        }
    }


    /**
     * Creates and returns a list of next generation seekers based on the current generation.
     */
    public Seeker[] createNextGenerationOfSeekers(
        LongUnaryOperator seekerIndexGenerator,
        UnaryOperator<Seeker> bestSeekerMutator,
        UnaryOperator<Seeker> trailingSeekersMutator
    ) {
        Seeker[] originSeekers                    = this.getSeekers();
        Seeker[] nextGenSeekers                   = new Seeker[originSeekers.length];
        Map<Integer, List<Seeker>> groupedSeekers = new HashMap<>();
        List<Integer> skills                      = new ArrayList<>();
        long cumulativeSkillValue                 = 0;
        int skill;
        int chosenSkill;
        int bestSkill;

        // group seekers by their skill
        for (Seeker seeker: originSeekers) {
            skill = seeker.getSkill();

            /*
             * The line below does the same as this:
             *
             * if (groupedSeekers.get(skill) == null) {
             *     groupedSeekers.put(skill, new ArrayList<>());
             * }
             */
            groupedSeekers.computeIfAbsent(skill, k -> new ArrayList<>());

            // add skill to cumulativeSkillValue and skills
            cumulativeSkillValue += skill;
            skills.add(skill);
            // add seeker to skill group
            groupedSeekers.get(skill).add(seeker);
        }

        this.generationSkill = cumulativeSkillValue;

        skills.sort((o1, o2) -> (int) Math.signum(o1 - o2));

        if (Config.SHOW_INFO) {
            this.visualizeGroupedSeekers(skills, groupedSeekers);
        }

        // store best skill for later comparison
        bestSkill = skills.get(skills.size() - 1);

        // store best seeker here without mutating it
        nextGenSeekers[0] = groupedSeekers.get(bestSkill).get(0).generateClone();

        nextGenSeekers[0].bestSeeker = true;

        // assign new seekers (do not mutate best seeker which is on position 0)
        for (int s = 1; s < nextGenSeekers.length; s++) {
            long seekerSkillIndex = seekerIndexGenerator.applyAsLong(cumulativeSkillValue);
            int skillPosition = (skills.size() - 1);
            long currentSkillValue = cumulativeSkillValue;
            Seeker seeker;

            // run until level of desired seeker is reached
            while ((currentSkillValue -= skills.get(skillPosition)) >= seekerSkillIndex) {
                skillPosition--;
            }

            chosenSkill = skills.get(skillPosition);
            seeker = groupedSeekers.get(chosenSkill).get(
                    (int) (Math.random() * groupedSeekers.get(chosenSkill).size())
            );

            // apply mutations to improve
            nextGenSeekers[s] = (this.treasureFound
                    ? bestSeekerMutator.apply(seeker)
                    : trailingSeekersMutator.apply(seeker)
            );
        }

        if (Config.SHOW_INFO) {
            this.visualizeSeekerSelection();
        }

        return nextGenSeekers;
    }


    //====================================================================================================
    //-------------------- Additional optional visualization methods below            --------------------
    //====================================================================================================

    private void visualizeGroupedSeekers(List<Integer> skills, Map<Integer, List<Seeker>> groupedSeekers) {
        int xPosition = 200;
        Info info = Info.getInstance();
        // filter out multiple skills
        List<Integer> filteredSkills = new ArrayList<>(Set.copyOf(skills));
        // restore order
        filteredSkills.sort((o1, o2) -> (int) Math.signum(o1 - o2));

        BiConsumer<PGraphics, InfoDrawing> biConsumer = (canvas, infoDrawing) -> {
            int listPosition = infoDrawing.value(0);
            List<Seeker> currentSeekers = groupedSeekers.get(filteredSkills.get(listPosition));
            String text = ("(" + currentSeekers.get(0).getSkill() + ") x " + currentSeekers.size());

            canvas.textSize(Config.INFO_DEFAULT_TEXT_SIZE);
            canvas.text(text, infoDrawing.x, infoDrawing.y);
        };

        // transition arrow
        info.addDrawing(new InfoDrawing(
                xPosition - 55,
                Config.INFO_HEADLINE_Y + 117,
                1000,
                this.visualizationArrow()
        ));

        // headline
        info.addDrawing(new InfoDrawing(
                xPosition,
                Config.INFO_HEADLINE_Y,
                500,
                (canvas, infoDrawing) -> {
                    canvas.textSize(Config.INFO_DEFAULT_TEXT_SIZE);
                    canvas.text("Sorted and grouped:", infoDrawing.x, infoDrawing.y);
                }
        ));

        // show best 10 groups
        for (int skillPosition = (filteredSkills.size() - 1), yOffset = 0;
             skillPosition >= (filteredSkills.size() - 11);
             skillPosition--, yOffset++
        ) {
            info.addDrawing(new InfoDrawing(
                    xPosition,
                    yOffset * Config.VISUALIZATION_OFFSET_FACTOR,
                    biConsumer,
                    skillPosition
            ));
        }
    }


    private void visualizeSeekerSelection() {
        Info info      = Info.getInstance();
        int defaultX   = 470;
        int defaultY   = Config.INFO_HEADLINE_Y + 2;
        int arrayX     = 60;
        int arrayY     = 320;
        int range      = 30;
        int[] skills   = new int[] {6, 5, 5, 4, 2, 2, 2, 2, 1, 1};
        Color[] colors = new Color[] {
                new Color(255, 0, 0),
                new Color(100, 100, 200),
                new Color(0, 255, 0),
                new Color(200, 100, 0),
                new Color(255, 255, 0),
                new Color(0, 255, 255),
                new Color(255, 0, 255),
                new Color(0, 0, 255),
                new Color(100, 100, 100),
                new Color(0, 0, 0)
        };
        Color color = colors[0];

        // headline available seekers
        info.addDrawing(new InfoDrawing(
                40,
                290,
                0,
                (canvas, infoDrawing) -> {
                    canvas.textSize(Config.INFO_DEFAULT_TEXT_SIZE);
                    canvas.text("Selection for next generation: ", infoDrawing.x, infoDrawing.y);
                }
        ));

        info.addDrawing(new InfoDrawing(
                40,
                arrayY,
                0,
                (canvas, infoDrawing) -> {
                    canvas.textSize(Config.INFO_DEFAULT_TEXT_SIZE);
                    canvas.text("[", infoDrawing.x, infoDrawing.y);
                    canvas.text("]", 660, infoDrawing.y);
                }
        ));

        // draw skills as bars
        for (int i = 0, bar = -1, remainingParts = 0; i < range; i++, remainingParts--) {
            if (remainingParts == 0) {
                bar++;
                remainingParts = skills[bar];
                color = colors[bar];
            }

            info.addDrawing(new InfoDrawing(
                    (defaultX + 35 + ((i / 10) * Config.VISUALIZATION_OFFSET_FACTOR * 3)),
                    (defaultY - 5  + ((i % 10) * Config.VISUALIZATION_OFFSET_FACTOR)),
                    0,
                    (canvas, infoDrawing) -> {
                        canvas.fill(infoDrawing.value(0), infoDrawing.value(1), infoDrawing.value(2));
                        canvas.rect(infoDrawing.x, infoDrawing.y, 10, Config.VISUALIZATION_OFFSET_FACTOR);
                    },
                    color.getRed(), color.getGreen(), color.getBlue()
            ));
        }

        // add (empty drawing with) delay before starting selection process
        info.addDrawing(new InfoDrawing(0, 0, 1000, (c, i) -> { }));

        // select random seekers (choose random seekerIndex)
        for (int pos = 1, currentPos, randomSeekerIndex, selected, seekerPos; pos <= skills.length; pos++) {
            randomSeekerIndex = (int) (Math.random() * range);
            currentPos = 0;

            // display randomSeekerIndex
            info.addDrawing(new InfoDrawing(
                    540,
                    230,
                    1250,
                    (canvas, infoDrawing) -> {
                        canvas.textSize(50);
                        canvas.text(
                                "" + infoDrawing.value(0),
                                infoDrawing.x,
                                infoDrawing.y
                        );
                        canvas.textSize(Config.INFO_DEFAULT_TEXT_SIZE);
                    },
                    (canvas, infoDrawing) -> { },
                    randomSeekerIndex
            ));

            // display selection "animation"
            do {
                info.addDrawing(new InfoDrawing(
                        (defaultX + ((currentPos / 10) * Config.VISUALIZATION_OFFSET_FACTOR * 3)),
                        (defaultY + ((currentPos % 10) * Config.VISUALIZATION_OFFSET_FACTOR)),
                        100,
                        this.visualizationArrow(),
                        (x, y) -> { }
                ));
                currentPos++;
            } while (currentPos <= randomSeekerIndex);

            currentPos--;

            // freeze last arrow for a little longer
            info.addDrawing(new InfoDrawing(
                    (defaultX + ((currentPos / 10) * Config.VISUALIZATION_OFFSET_FACTOR * 3)),
                    (defaultY + ((currentPos % 10) * Config.VISUALIZATION_OFFSET_FACTOR)),
                    500,
                    this.visualizationArrow(50, 205, 50),
                    (x, y) -> { }
            ));

            currentPos = 0;
            seekerPos = 0;

            do {
                currentPos += skills[seekerPos];
                selected = seekerPos;
                seekerPos++;
            } while (currentPos <= randomSeekerIndex);

            // add seeker to "array" in visualization
            info.addDrawing(new InfoDrawing(
                    (arrayX + (int) ((pos - 1) * Config.VISUALIZATION_OFFSET_FACTOR * 2.9)),
                    arrayY,
                    250,
                    (canvas, infoDrawing) -> {
                        String str = ("s" + infoDrawing.value(0) + ",");
                        canvas.fill(infoDrawing.value(1), infoDrawing.value(2), infoDrawing.value(3));
                        canvas.text(str, infoDrawing.x, infoDrawing.y);
                        canvas.fill(0);
                    },
                    pos,
                    colors[selected].getRed(),
                    colors[selected].getGreen(),
                    colors[selected].getBlue()
            ));
        }

        info.addDrawing(new InfoDrawing(
                (arrayX + (int) (skills.length * Config.VISUALIZATION_OFFSET_FACTOR * 2.85)),
                arrayY,
                (canvas, infoDrawing) -> canvas.text("...", infoDrawing.x, infoDrawing.y),
                (skills.length + 1)
        ));
    }


    private BiConsumer<PGraphics, InfoDrawing> visualizationArrow() {
        return this.visualizationArrow(0, 0, 0);
    }


    private BiConsumer<PGraphics, InfoDrawing> visualizationArrow(int red, int green, int blue) {
        return (canvas, infoDrawing) -> {
            canvas.fill(red, green, blue);
            canvas.stroke = false;
            canvas.rect(infoDrawing.x, infoDrawing.y, 25, 10);
            canvas.triangle(
                    infoDrawing.x + 35, infoDrawing.y + 5,
                    infoDrawing.x + 25, infoDrawing.y - 5,
                    infoDrawing.x + 25, infoDrawing.y + 15
            );
            canvas.stroke = true;
            canvas.fill(0);
        };
    }


    @Override
    public String toString() {
        return "Generation: " + this.getNo();
    }
}

import processing.core.PGraphics;

import java.util.function.BiConsumer;

public class InfoDrawing {

    public final int x;
    public final int y;
    public final int sleepTime;
    private BiConsumer<PGraphics, InfoDrawing> biConsumer;
    private BiConsumer<PGraphics, InfoDrawing> after;
    private int[] additionalValues;

    public InfoDrawing(
            int x,
            int y,
            BiConsumer<PGraphics, InfoDrawing> biConsumer,
            int... additionalValues
    ) {
        this(
                x,
                y,
                Config.DEFAULT_INFO_TIMEOUT,
                biConsumer,
                additionalValues
        );
    }

    public InfoDrawing(
            int x,
            int y,
            int sleepTime,
            BiConsumer<PGraphics, InfoDrawing> biConsumer,
            int... additionalValues
    ) {
        this(
                x,
                y,
                sleepTime,
                biConsumer,
                null,
                additionalValues
        );
    }

    public InfoDrawing(
            int x,
            int y,
            BiConsumer<PGraphics, InfoDrawing> biConsumer,
            BiConsumer<PGraphics, InfoDrawing> after,
            int... additionalValues
    ) {
        this(
                x,
                y,
                Config.DEFAULT_INFO_TIMEOUT,
                biConsumer,
                after,
                additionalValues
        );
    }

    public InfoDrawing(
            int x,
            int y,
            int sleepTime,
            BiConsumer<PGraphics, InfoDrawing> biConsumer,
            BiConsumer<PGraphics, InfoDrawing> after,
            int... additionalValues
    ) {
        this.x                = x;
        this.y                = y;
        this.sleepTime        = sleepTime;
        this.biConsumer       = biConsumer;
        this.after            = after;
        this.additionalValues = additionalValues;
    }

    public BiConsumer<PGraphics, InfoDrawing> getBiConsumer() {
        return this.biConsumer;
    }

    public BiConsumer<PGraphics, InfoDrawing> getFinishedAnimation() {
        assert this.isAnimation();
        return this.after;
    }

    public boolean isAnimation() {
        return this.after != null;
    }

    public int value(int index) {
        return additionalValues[index];
    }

    @Override
    public String toString() {
        return "InfoDrawing - animated: " + isAnimation();
    }
}

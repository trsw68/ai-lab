import processing.core.PApplet;

import java.util.concurrent.TimeUnit;


/**
 * The ai lab main class.
 */
public class Main extends PApplet {

    /**
     * The world of this visualization.
     */
    private World world;


    /**
     * Tracks the status of additional information visualization. Configurable in Config.java.
     * TRUE:  Information between generations is currently being shown.
     * FALSE: Visualization of the world / generations / seekers is running.
     */
    private boolean showInfo;


    /**
     * Draws the visualization for processes between generations.
     */
    private void drawInfo() {
        if (Info.getInstance().finishedDrawingContent()) {
            Info.getInstance().reset();
            this.showInfo = false;
            this.world.showPreviousGeneration(false);
            try {
                TimeUnit.SECONDS.sleep(Config.INFO_TIMEOUT_AFTER_DRAWING);
            } catch (InterruptedException ignored) { }
        } else {
            this.drawWorld();
            this.translate(this.world.width() / 2, this.world.getInformationYOffset() * 3);
            Info.getInstance().draw(this.g);
            this.translate(-this.world.width() / 2, -this.world.getInformationYOffset() * 3);
        }
    }


    /**
     * Updates and draws the visualization of the world.
     */
    private void drawAndUpdateWorld() {
        this.world.update();
        this.drawWorld();
        this.renderFPS();

        // creates a new generation if current generation is dead
        if (this.world.currentGeneration().isDead()) {
            this.world.createNewGeneration();
            if (Config.SHOW_INFO) {
                this.showInfo = true;
                this.world.showPreviousGeneration(true);
            }
        }
    }


    private void drawWorld() {
        this.world.draw(this.g);
    }


    /**
     * Renders the current frame rate.
     */
    private void renderFPS() {
        this.fill(0);
        this.textSize(20);

        this.text(
            (int) frameRate + " fps",
            this.width - 80, // x
            this.world.getInformationYOffset() // y
        );
    }


    @Override
    public void keyPressed() {
        if (this.key == 'i') {
            Config.SHOW_INFO = !Config.SHOW_INFO;
        }
    }


    /**
     * Setups some relevant processing parameters.
     */
    public void setup() {
        this.frameRate(Config.FRAME_RATE);
    }


    /**
     * Setups some relevant settings.
     */
    public void settings() {
        // load images here because world creation requires the images already
        Images.getInstance().addImage(loadImage("beemin.png"), "bee");
        Images.getInstance().addImage(loadImage("flowermin.png"), "treasure");
        Images.getInstance().addImage(loadImage("tree1.png"), "tree1");
        Images.getInstance().addImage(loadImage("tree2.png"), "tree2");
        Images.getInstance().addImage(loadImage("tree3.png"), "tree3");
        Images.getInstance().addImage(loadImage("grassmin.png"), "path");

        this.world = new World();
        size(this.world.width(), this.world.height()); // "P2D" not in class path
    }


    /**
     * Called on each frame to draw the contents on the screen.
     */
    public void draw() {
        background(
                Config.CANVAS_BACKGROUND_COLOR.getRed(),
                Config.CANVAS_BACKGROUND_COLOR.getGreen(),
                Config.CANVAS_BACKGROUND_COLOR.getBlue()
        );

        if (Config.SHOW_INFO) {
            fill(0, 255, 0);
        } else {
            fill(255, 0, 0);
        }

        this.g.stroke = false;
        this.circle((this.world.width() / 2) + 130, 329, 10);
        this.fill(0);
        this.text("Show info:", (this.world.width() / 2) + 10, 337);
        this.textSize(Config.INFO_DEFAULT_TEXT_SIZE * 0.8f);
        this.text("( Toggle: i )", (this.world.width() / 2) + 10, 357);
        this.textSize(Config.INFO_DEFAULT_TEXT_SIZE);
        this.g.stroke = true;

        if (this.showInfo) {
            this.drawInfo();
        } else {
            this.drawAndUpdateWorld();
        }
    }
}

/**
 * All available maze tile types.
 */
public enum MazeTileType {
    WALL,
    PATH,
    TREASURE,
    SEEKERS_START_POINT
}

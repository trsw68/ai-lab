import java.awt.*;

/**
 * Important values are defined here.
 */
public class Config {

    //====================================================================================================
    //--- Feel free to change the values below.                                                        ---
    //====================================================================================================

    /**
     * The used maze.
     *
     * Available mazes:
     * - Maze.MULTI_PATH (DEFAULT)
     * - Maze.SINGLE_PATH
     * - Maze.ANGULAR
     * - Maze.OPEN_SPACE
     * - Maze.OPEN_SPACE_WITH_WALL
     */
    public static final Maze MAZE = Maze.MULTI_PATH;


    /**
     * Defines whether additional information about the generation process
     * of a new generation from the previous generation should be displayed.
     * Toggleable with the 'i' key while the visualization is running.
     */
    public static boolean SHOW_INFO = false;


    //====================================================================================================
    //--- Feel free to experiment a bit with the values below.                                         ---
    //--- If you are not exactly sure what you are doing, it is recommended to leave them as they are. ---
    //====================================================================================================

    /**
     * The maximum frame rate of the visualization.
     *
     * Recommended range: 30 - 100 (DEFAULT: 60)
     */
    public static final int FRAME_RATE = 60;


    /**
     * The count of seekers of each created generation.
     *
     * Recommended range: 200 - 5000 (DEFAULT: 500)
     */
    public static final int SEEKER_COUNT = 500;


    /**
     * The number of move counts of each seeker.
     *
     * Recommended range: 350 - 1000 (DEFAULT: 500)
     */
    public static final int SEEKER_MOVE_COUNT = 500;


    /**
     * The permutation rate of a seeker.
     *
     * Recommended range: 0.005 - 0.1 (DEFAULT: 0.03)
     */
    public static final double DEFAULT_SEEKER_PERMUTATION_RATE = 0.03;


    //====================================================================================================
    //---------! It is strongly recommended to leave the values below as they are.                !-------
    //---------! Changing values below will probably lead to the program not working as intended. !-------
    //====================================================================================================

    /**
     * The maximum move intention value of a direction.
     */
    public static final float SEEKER_MAX_MOVE_INTENTION = 2f;


    /**
     * The maximum velocity of a seeker.
     */
    public static final int SEEKER_MAX_VELOCITY = 4;


    /**
     * The maze tile dimension for both x and y axis.
     */
    public static final int MAZE_TILE_DIMENSION = 10;


    /**
     * The default skill of a seeker.
     */
    public static final int SEEKER_DEFAULT_SKILL = -1;


    /**
     * The default size of info texts.
     */
    public static final int INFO_DEFAULT_TEXT_SIZE = 20;


    /**
     * The default size of info graphics.
     */
    public static final int INFO_DEFAULT_SIZE = (Config.INFO_DEFAULT_TEXT_SIZE / 4);


    /**
     * The timeout in seconds after the drawing process of additional info graphics has finished.
     */
    public static final int INFO_TIMEOUT_AFTER_DRAWING = 10;


    /**
     * The default timeout between each step of the info graphic drawing process.
     */
    public static final int DEFAULT_INFO_TIMEOUT = 250;


    /**
     * The default factor / spacing between each info graphic.
     */
    public static final int VISUALIZATION_OFFSET_FACTOR = 20;


    /**
     * The default y position of info headlines.
     */
    public static final int INFO_HEADLINE_Y = (int) -(Config.VISUALIZATION_OFFSET_FACTOR * 1.5);


    /**
     * The background color of the canvas.
     */
    public static final Color CANVAS_BACKGROUND_COLOR = new Color(255, 255, 255);
}

# AI Lab

## Team

- Jens Hansen
- Tim Robin Schweizer


## Preview

![preview](doc/preview.jpg)


## IntelliJ Konfiguration

Da das Projekt auf Basis von **Processing** erstellt wurde, muss zum Starten des Programms die **Processing Core Library** eingebunden werden:

https://processing.org/download/

![project_structure](doc/project_structure_library.png)



Zum Starten muss dann noch die **Run Configuration** folgendermaßen konfiguriert werden.

![run_configuration](doc/run_configuration.png)


## Some Sketches

Auf Wunsch sind hier noch ein paar Dokumente die wir während der Entwicklung erstellt hatten.

- [Planung](doc/sketch1.jpg)
- [Next Seeker Generation](doc/sketch2.jpg)
- [Skill Calculation #1](doc/sketch3.jpg)
- [Skill Calculation #2](doc/sketch4.jpg)
- [Velocity](doc/velocity.jpg)
- [Wall collision](doc/wall_collision.jpeg)


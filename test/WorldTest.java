import org.junit.Test;

import static org.junit.Assert.*;

public class WorldTest {

    @Test
    public void getCurrentGeneration() {
        World world = new World();

        assertNotNull("World has not generated a generation", world.currentGeneration());
    }
}
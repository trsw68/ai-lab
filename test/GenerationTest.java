import org.junit.Assert;
import org.junit.Test;
import processing.core.PVector;

import java.util.Iterator;
import java.util.List;

public class GenerationTest {

    /**
     * A simple test maze.
     */
    private Maze testMaze = Maze.SINGLE_PATH;


    @Test
    public void createNewGeneration() {
        Generation firstGeneration  = new Generation(this.testMaze.getSeekersStartPoint());
        Generation secondGeneration = firstGeneration.createNextGeneration(this.testMaze);
        Generation thirdGeneration  = secondGeneration.createNextGeneration(this.testMaze);

        assert firstGeneration.getNo()  == 1;
        assert secondGeneration.getNo() == 2;
        assert thirdGeneration.getNo()  == 3;

        Assert.assertNotEquals("first generation is also second generation", firstGeneration, secondGeneration);
        Assert.assertNotEquals("first generation is also third generation", firstGeneration, thirdGeneration);
    }


    @Test
    public void getSeekers() {
        Assert.assertTrue("seeker count is 0", new Generation(testMaze.getSeekersStartPoint()).getSeekers().length > 0);
    }


    @Test
    public void createNextGenerationSeekers() {
        PVector startingPosition = this.testMaze.getSeekersStartPoint();
        Seeker testSeeker1       = new Seeker(startingPosition, new PVector[]{ }, 2);
        Seeker testSeeker2       = new Seeker(startingPosition, new PVector[]{ }, 5);
        Seeker testSeeker3       = new Seeker(startingPosition, new PVector[]{ }, 6);
        Seeker testSeeker4       = new Seeker(startingPosition, new PVector[]{ }, 7);
        Seeker testSeeker5       = new Seeker(startingPosition, new PVector[]{ }, 6);
        Seeker testSeeker6       = new Seeker(startingPosition, new PVector[]{ }, 1);
        Generation generation1   = new Generation(
            1,
            startingPosition,
            new Seeker[] {
                testSeeker1,
                testSeeker2,
                testSeeker3,
                testSeeker4,
                testSeeker5,
                testSeeker6
            }
        );
        Iterator<Long> testSeekerIndexIterator = List.of(2L, 5L, 13L, 21L, 18L, 10L).iterator();
        Seeker[] nextGenSeekers                = generation1.createNextGenerationOfSeekers(
            cumulativeSkillValue -> testSeekerIndexIterator.next(), // creates testable seeker indices
            seeker -> seeker, // no mutation for test
            seeker -> seeker  // no mutation for test
        );

        // 2 + 5 + 6 + 7 + 6 = 26

        //  0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 |
        //excl    |  sk1 2|       sk2 (5)     |           sk3 (6)                    sk5 (6)             |                 sk4 (7)          |

        //Assert.assertEquals("first element not match", nextGenSeekers[0], testSeeker1);
        Assert.assertEquals("second element mismatch", nextGenSeekers[1], testSeeker1);
        Assert.assertEquals("third element mismatch", nextGenSeekers[2], testSeeker2);
        Assert.assertTrue(
            "fourth element mismatch",
            nextGenSeekers[3].equals(testSeeker3) || nextGenSeekers[3].equals(testSeeker5)
        );
        Assert.assertEquals("third element mismatch", nextGenSeekers[4], testSeeker4);
        Assert.assertTrue(
                "sixth element mismatch",
                nextGenSeekers[5].equals(testSeeker3) || nextGenSeekers[5].equals(testSeeker5)
        );
    }
}
